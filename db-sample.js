var database = require('./src/data/database');
var flightData = require('./src/agency/test/flights');

database.connect().then(() => {
  database.drop();
  database.fixture(flightData).then(() => {
    console.log('create fixture successfully.');
    process.exit(0);
  }).catch(() => {
    console.error('error');
    process.exit(-1);
  });
})
