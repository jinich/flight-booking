const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const gulp = require('gulp');
const gutil = require('gulp-util');
const nodemon = require('nodemon');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');

gulp.task('default', ['mongo', 'client', 'sass:watch', 'server']);
gulp.task('all', ['mongo', 'client', 'sass:watch', 'nodemon']);

gulp.task('test', ['mongo', 'mocha']);

gulp.task('mocha', function() {
  spawn('npm.cmd', ['run', 'mocha'], {stdio: 'inherit'});
});

gulp.task('mongo', function() {
  exec('mongod --dbpath ./content/data', function(err, stdout, stderr) {
    if (err) {
      console.error(err);
    } else {
      console.log(stdout);
      console.log(stderr);
    }
  });
});

gulp.task('server', function() {
  spawn('npm.cmd', ['run', 'babel-node'], {stdio: 'inherit'});
});

gulp.task('nodemon', function() {
  spawn('npm.cmd', ['run', 'server'], {stdio: 'inherit'});
});

gulp.task('client', function() {
  spawn('npm.cmd', ['run', 'client'], {stdio: 'inherit'});
});

gulp.task('sass', function() {
  return gulp.src('./src/assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'))
    .pipe(browserSync.stream());
});

gulp.task('sass:watch', function() {
  browserSync.init({
    proxy: '127.0.0.1:3000',
    port: 3001
  });
  gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
});
