# Seat selection
## Primary actor: User
## Trigger: User choose 'Select seat' after fill in passengers information
## Preconditions:
* User already choose their flights and provided valid passengers information
## Postconditions:
Success Guarantees:
* Each passengers can only have one seat
* Seat is marked as PENDING status so other user can't select it
