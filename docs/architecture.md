# Thiết kế
## Thiết kế dữ liệu
#### User - Người dùng
* **email**: Phải là email hợp lệ và không có trong CSDL
* **password**: Gồm ít nhất 8 kí tự  _0-9, a-z, A-Z và một vài kí tự đặc biệt_
* **title**: MR hoặc MRS
* **first name**
* **last name**
* **admin**: true hoặc false
* phone number: Tối thiểu 4 chữ số từ _0-9_
* address

#### Flight - Chuyến bay
* **flight code**: Chính xác 5 kí tự _0-9, A-Z_
* **departure date**: dd/mm/yyyy
* **departure time**: hh:mm
* **arrival time**: hh:mm
* **arrival**: Chính xác 3 kí tự _A-Z_
* **departure**: Chính xác 3 kí tự _A-Z_
* **status**: Hết vé hoặc còn vé
* **class**: Phổ thông hoặc Thương gia
* **capacity**: Tối đa 525 chỗ, tối thiểu 0 chỗ
* **price**: Tối thiểu 0 (VNĐ)

#### Airport - Sân bay
* **airport code**: Chính xác 3 kí tự _A-Z_
* **airport name**

#### Booking - Đặt chỗ
* **booking code**: 6 kí tự _0-9, A-Z_
* **user _id**
* **booking time**: dd/mm/yyyy hh:mm:ss
* **total**: >= 0
* **status**: Hoàn tất hoặc đang đặt chỗ

#### Passenger - Hành khách
* **booking code**: 6 kí tự _0-9, A-Z_
* **title**: MR hoặc MRS
* **first name**
* **last name**

#### Flight Detail - Chặng bay
* **booking code**: 6 kí tự _0-9, A-Z_
* **flight code**: Chính xác 5 kí tự _0-9, A-Z_
* **departure date**: dd/mm/yyyy
* **class**: Phổ thông hoặc Thương gia
