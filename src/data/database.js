// for more information about database configuration for testing,
// visit https://www.terlici.com/2014/09/15/node-testing.html

const MongoClient = require('mongodb').MongoClient;
const config = require('../config');

let flightBookingDb = null;

// connect to mongodb database, with 2 modes: production or test
// database URI is retrieved from ./config.js

function resolveDbUri() {
  let mode = process.env.NODE_ENV;
  let dbUri = '';
  switch (mode) {
    case 'production':
    dbUri = config.production.dbUri;
    break;
    case 'development':
    dbUri = config.development.dbUri;
    break;
    case 'test':
    dbUri = config.test.dbUri;
    break;
    default:
    dbUri = config.development.dbUri;
    break;
  }
  return dbUri;
}

function connect() {
  if (isConnected()) {
    return Promise.resolve();
  }

  // choose database URI base on mode
  let uri = resolveDbUri();

  // connect
  return new Promise((resolve, reject) => {
    MongoClient.connect(uri, (err, db) => {
      if (!err) {
        flightBookingDb = db;
        resolve();
      } else {
        reject(err);
      }
    });
  });
}

// rest of application can access mongodb db object through this function
function getDb() {
  return flightBookingDb;
}

// drop database
// after completed a test, you need destroy test database to avoiding conflict
function drop() {
  flightBookingDb.dropDatabase();
}

// add collection to database from fixture (json file contain test data)
// userful for testing
function fixture(collection) {
  if (!isConnected()) {
    return Promise.reject("Missing database connection!");
  }

  let collectionName = collection.name;
  let documents = collection.documents;
  return flightBookingDb.collection(collectionName).insertMany(documents);
}

// check if database is connected
function isConnected() {
  return flightBookingDb !== null;
}

module.exports = {
  connect,
  getDb,
  drop,
  fixture
};
