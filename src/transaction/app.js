import express from 'express';
import bookingManager from './booking-manager';

let router = express.Router();

router.get('/:booking_id', bookingManager.findBooking);
router.get('/:booking_id/flight_details', bookingManager.findFlightDetails);
router.put('/create', bookingManager.createBooking);
router.post('/:booking_id/update_status/:status', bookingManager.updateStatus);
router.post('/:booking_id/add_flight_detail', bookingManager.addFlightDetail);

export default function bookingApi(app) {
  app.use('/api/booking', router);
}
