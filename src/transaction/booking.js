const database = require('../utils/database');

const BOOKING_COLLECTION = 'booking';
const BOOKING_STATUS_PENDING = 0;
const BOOK_STATUS_APPROVED = 1;
const NOT_FOUND = 'resource not found';
const UPDATED = 'updated';

module.exports = {
  NOT_FOUND,
  UPDATED,
  BOOKING_STATUS_PENDING,
  BOOK_STATUS_APPROVED,
  createBooking: () => {
    let bookingId = generateBookingId();
    let bookingStatus = BOOKING_STATUS_PENDING;
    let bookingDate = now();

    let bookingDoc = {
      booking_id: bookingId,
      status: bookingStatus,
      time: bookingDate,
      cost: 0,
      flight_details: [],
      passengers: [],
    };

    let bookingCollection = database.getDb().collection(BOOKING_COLLECTION);
    return new Promise((resolve, reject) => {
      bookingCollection.insertOne(bookingDoc, (err, result) => {
        console.log(result.toJSON())
        resolve(err, bookingDoc);
      });
    });
  },

  findBookingInfo: (bookingId) => {
    let db = database.getDb();
    return new Promise((resolve, reject) => {
      db.collection(BOOKING_COLLECTION).findOne({ booking_id: bookingId }, (err, booking) => {
        if (booking !== null) {
          resolve(err, booking);
        } else {
          reject(err, NOT_FOUND);
        }
      });
    });
  },

  updateBookingStatus: function(bookingId, status) {
    let db = database.getDb();
    return new Promise((resolve, reject) {
      db.collection(BOOKING_COLLECTION).update(
        { booking_id: bookingId },
        { $set: { status: status } },
        function(err, result) {
          if (result.toJSON().nModified > 0) {
            resolve(err, UPDATED);
          } else {
            reject(err, NOT_FOUND);
          }
        }
      );
    });
  },

  findPassengersInFlight: function(flightId, callback) {
    let db = database.getDb();
    db.collection(BOOKING_COLLECTION)
      .find({ 'flight_details.flight_id': { $eq: flightId } })
      .toArray(function(err, bookings) {
        if (bookings.length > 0) {
          let passengers = [];
          for (let i = 0; i < bookings.length; i++) {
            let booking = bookings[i];
            let passengersPerBooking = booking.passengers;
            passengers = passengers.concat(passengersPerBooking);
          }
          callback(err, passengers);
        } else {
          callback(err, NOT_FOUND);
        }
      });
  },

  findAllPassengers: function(callback) {
    let db = database.getDb();
    db.collection(BOOKING_COLLECTION)
      .find()
      .toArray(function(err, bookings) {
        let passengers = [];
        for (let i = 0; i < bookings.length; i++) {
          let booking = bookings[i];
          let passengersPerBooking = booking.passengers;
          passengers = passengers.concat(passengersPerBooking);
        }
        callback(err, passengers);
      });
  },

  addNewPassenger: function(bookingId, passenger, callback) {
    let db = database.getDb();
    db.collection(BOOKING_COLLECTION).updateOne(
      { booking_id: bookingId },
      { $push: { passengers: passenger } },
      function(err, result) {
        if (result.toJSON().nModified > 0) {
          callback(err, UPDATED);
        } else {
          callback(err, NOT_FOUND);
        }
      }
    );
  },

  findFlightDetails: function(bookingId, callback) {
    let db = database.getDb();
    db.collection(BOOKING_COLLECTION).findOne({ booking_id: bookingId }, function(err, booking) {
      if (booking !== null) {
        let flightDetails = booking.flight_details;
        callback(err, flightDetails);
      } else {
        callback(err, NOT_FOUND);
      }
    });
  },

  addFlightDetail: function(bookingId, flightDetail, callback) {
    let db = database.getDb();
    db.collection(BOOKING_COLLECTION).updateOne(
      { booking_id: bookingId },
      { $push: { flight_details: flightDetail } },
      function(err, result) {
        if (result.toJSON().nModified > 0) {
          callback(err, UPDATED);
        } else {
          callback(err, NOT_FOUND);
        }
      }
    );
  },
};

function now() {
  let date = new Date();
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let hour = date.getHours();
  let minute = date.getMinutes();
  let second = date.getSeconds();
  return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
}

function generateBookingId() {
  let letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let code = '';
  let codeLength = 6;
  for (let i = 0; i < 6; i++) {
    let randomPos = getRandomNumber(0, letters.length - 1);
    let character = letters.charAt(randomPos);
    code += character;
  }
  return code;
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
