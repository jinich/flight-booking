import jwt from 'jsonwebtoken';
import config from '../config';
import database from '../data/database';
import validateSignupInfo from '../utils/user-validator';
import addUser from '../utils/signup';
import errorCode from '../utils/error-code';
import response from '../utils/response';

function generateAdminToken(admin) {
  let token = jwt.sign(admin, config.secret, { expiresIn: '24h' });
  return token;
}

function createSuperuser(user) {
  let collection = database.getDb().collection('users');
  if (validateSignupInfo(user)) {
    let superuser = Object.create(user);
    superuser.admin = true;
    return addUser(superuser, collection);
  } else {
    return Promise.reject({ code: errorCode.NOT_FOUND_ERROR });
  }
}

function auth(username, password) {
  let collection = database.getDb().collection('users');
  let cursor = collection.find({ username, password }).limit(1);
  return cursor.hasNext().then((exist) => {
    if (exist) {
      return cursor.next();
    } else {
      return Promise.reject({ code: errorCode.NOT_FOUND_ERROR });
    }
  }).then((user) => {
    if (user.admin) {
      let token = generateAdminToken(user);
      return token;
    } else {
      return Promise.reject({ code: errorCode.PERMISSION_ERROR });
    }
  });
}

function login(req, res) {
  let username = req.body.username;
  let password = req.body.password;

  auth(username, password).then((token) => {
    response.success(res, {
      token,
      message: 'Hello, Administrator!'
    });
  }).catch((err) => {
    switch (err.code) {
    case errorCode.NOT_FOUND_ERROR:
      response.notFoundError(res, 'User not found.');
      break;
    case errorCode.PERMISSION_ERROR:
      response.forbiddenError(res);
      break;
    case errorCode.INTERNAL_ERROR:
      response.internalError(res);
      break;
    default:
      response.internalError(res);
      break;
    }
  });
}

export { createSuperuser, login };
