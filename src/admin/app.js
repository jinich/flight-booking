import express from 'express';
import { login } from './superuser';

let router = express.Router();
router.post('/authenticate', login);

export default function setupAdminApp(app) {
  app.use('/api/admin', router);
}
