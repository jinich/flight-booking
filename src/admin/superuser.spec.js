import request from 'request';
import { expect } from 'chai';
import config from '../config';
import * as su from './superuser';
import app from '../app';
import database from '../data/database';

describe('ADMIN API', () => {
  const url = `${config.test.host}:${config.test.port}`;

  before((done) => {
    Promise.all([database.connect(), app.start()]).then(([status, port]) => {
      console.log('Server is listening on port', port);
      done();
    }).catch((err) => {
      console.log(err);
    });
  });

  it('should create a new superuser', (done) => {
    let admin = {
      username: 'jinich',
      password: '12345678'
    };
    su.createSuperuser(admin).then((user) => {
      expect(user.username).to.equal(admin.username);
      done();
    }).catch((err) => {
      console.log(err);
      done();
    });
  });

  it('should auth as admin', (done) => {
    let username = 'jinich';
    let password = '12345678';
    request({
      url: `${url}/api/admin/authenticate`,
      method: 'POST',
      dataType: 'json',
      json: {
        username,
        password
      }
    }, (err, response, body) => {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  after(() => {
    app.stop();
    database.drop();
  });
});
