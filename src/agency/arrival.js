import database from '../data/database';

function findArrivals(req, res) {
  let airportCode = req.params.departure_airport_code;
  let flightCollection = database.getDb().collection('flights');
  let arrivalField = 'arrival';
  flightCollection.distinct(arrivalField, { 'departure.airportCode': airportCode }).then((arrivals) => {
    if (arrivals.length > 0) {
      res.json(arrivals);
    } else {
      res.sendStatus(404);
    }
  }).catch(() => res.sendStatus(400));
}

export default findArrivals;
