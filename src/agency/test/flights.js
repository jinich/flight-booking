const flights = {
  name: 'flights',
  documents: [
    {
      code: 'SQ 172',
      departure: {
        airportCode: 'SIN',
        airportName: 'Changi Intl',
        time: '9:50',
        date: new Date(2016, 10, 20), // tháng 11 :'(
        country: 'Singapore',
        city: 'Singapore'
      },
      arrival: {
        airportCode: 'SGN',
        airportName: 'Tan Son Nhat',
        time: '19:00',
        date: new Date(2016, 10, 20),
        country: 'Vietnam',
        city: 'Ho Chi Minh City'
      },
      class: {
        economy: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        economyFlexi: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        business: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        first: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        }
      },
      totalSlot: 100,
      availableSlot: 100,
      operator: 'Singapore Airlines',
      totalTravelTime: '2hrs 10mins',
      aircraft: {
        name: 'Airbus',
        code: 'A330-300'
      }
    },
    {
      code: 'SQ 173',
      departure: {
        airportCode: 'CTS',
        airportName: 'Chitose',
        time: '9:50',
        date: new Date(2016, 10, 20),
        country: 'Japan',
        city: 'Sapporo'
      },
      arrival: {
        airportCode: 'SGN',
        airportName: 'Tan Son Nhat',
        time: '10:45',
        date: new Date(2016, 10, 21),
        country: 'Vietnam',
        city: 'Ho Chi Minh City'
      },
      class: {
        economy: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        economyFlexi: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        business: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        first: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        }
      },
      totalSlot: 100,
      availableSlot: 100,
      operator: 'Singapore Airlines',
      totalTravelTime: '2hrs 10mins',
      aircraft: {
        name: 'Airbus',
        code: 'A330-300'
      }
    },
    {
      code: 'SQ 174',
      departure: {
        airportCode: 'SGN',
        airportName: 'Tan Son Nhat',
        time: '19:00',
        date: new Date(2016, 10, 27),
        country: 'Vietnam',
        city: 'Ho Chi Minh City'
      },
      arrival: {
        airportCode: 'SIN',
        airportName: 'Changi Intl',
        time: '9:50',
        date: new Date(2016, 10, 27), // tháng 11 :'(
        country: 'Singapore',
        city: 'Singapore'
      },
      class: {
        economy: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        economyFlexi: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        business: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        first: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        }
      },
      totalSlot: 100,
      availableSlot: 100,
      operator: 'Singapore Airlines',
      totalTravelTime: '2hrs 10mins',
      aircraft: {
        name: 'Airbus',
        code: 'A330-300'
      }
    },
    {
      code: 'SQ 172',
      departure: {
        airportCode: 'SIN',
        airportName: 'Changi Intl',
        time: '9:50',
        date: new Date(2016, 10, 20), // tháng 11 :'(
        country: 'Singapore',
        city: 'Singapore'
      },
      arrival: {
        airportCode: 'CTS',
        airportName: 'Chitose',
        time: '9:50',
        date: new Date(2016, 10, 20),
        country: 'Japan',
        city: 'Sapporo'
      },
      class: {
        economy: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        economyFlexi: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        business: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        },
        first: {
          totalSlot: 25,
          availableSlot: 25,
          standardFare: 197.2,
          childFare: 100,
          infantFare: 20,
        }
      },
      totalSlot: 100,
      availableSlot: 100,
      operator: 'Singapore Airlines',
      totalTravelTime: '2hrs 10mins',
      aircraft: {
        name: 'Airbus',
        code: 'A330-300'
      }
    }
  ]
};

module.exports = flights;
