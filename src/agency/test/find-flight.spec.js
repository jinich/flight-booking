import request from 'request';
import { expect } from 'chai';
import config from '../../config';
import app from '../../app';
import database from '../../data/database';
import flightFixture from './flights';

describe('FIND FLIGHT API', () => {
  const url = `${config.test.host}:${config.test.port}`;

  before((done) => {
    Promise.all([database.connect(), database.fixture(flightFixture), app.start()]).then(([status, insertStatus, port]) => {
      console.log('Server is listening on port', port);
      done();
    }).catch((err) => {
      console.log(err);
    });
  });

  it("get all departures", (done) => {
    request.get(`${url}/api/flights/departures`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("get all arrivals corresponse to departure", (done) => {
    request.get(`${url}/api/flights/arrivals/SIN`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("not found any arrivals", (done) => {
    request.get(`${url}/api/flights/arrivals/KKK`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(404);
      done();
    });
  });

  it("return available flight", (done) => {
    request.get(`${url}/api/flights?departure=SGN&arrival=SIN&date=27/11/2016&class=Economy&flexible=false&passengers=2`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("search for flights in 7 days (flexible travel day / lowest fare)", (done) => {
    request.get(`${url}/api/flights?departure=SIN&arrival=SGN&date=20/11/2016&class=Economy&flexible=true&passengers=2`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it("view schedules", (done) => {
    request.get(`${url}/api/flights/schedule?departure=SIN&arrival=SGN&date=20/11/2016&class=Economy&passengers=2`, (err, response, body) => {
      expect(err).to.be.null;
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  after(() => {
    app.stop();
    database.drop();
  })
});
