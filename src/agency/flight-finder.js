import moment from 'moment';
import database from '../data/database';

function findFlights(req, res) {
  let departure = req.query.departure;
  let arrival = req.query.arrival;
  let fareClass = req.query.class;
  let date = moment(req.query.date, 'DD/MM/YYYY').toDate();
  let passengers = parseInt(req.query.passengers);
  let flexible = req.query.flexible;

  let departureDate = date;
  if (flexible === 'true') {
    let startDate = moment(date).subtract(3, 'days').toDate();
    let endDate = moment(date).add(3, 'days').toDate();
    departureDate = {
      $gte: startDate,
      $lte: endDate
    };
  }

  let fareClassQueries = [];
  switch (fareClass) {
  case 'Economy':
    fareClassQueries.push({
      'class.economy.availableSlot': { $gte: passengers }
    }, {
      'class.economyFlexi.availableSlot': { $gte: passengers }
    });
    break;
  case 'Business':
    fareClassQueries.push({
      'class.business.availableSlot': { $gte: passengers }
    });
    break;
  case 'First':
    fareClassQueries.push({
      'class.first.availableSlot': { $gte: passengers }
    });
    break;
  default:
    fareClassQueries.push({
      'class.economy.availableSlot': { $gte: passengers }
    });
    fareClassQuery.push({
      'class.economyFlexi.availableSlot': { $gte: passengers }
    });
    break;
  }

  let query = {
    $and: [
      { 'departure.airportCode': departure },
      { 'arrival.airportCode': arrival },
      { 'departure.date': departureDate },
      { $or: fareClassQueries }
    ]
  };

  database.getDb().collection('flights').find(query).toArray().then(flights => {
    if (flights.length > 0) {
      res.json(flights);
    } else {
      res.sendStatus(404);
    }
  }).catch(() => res.sendStatus(400));
}

function findFlightSchedules(req, res) {
  let departure = req.query.departure;
  let arrival = req.query.arrival;
  let fareClass = req.query.class;
  let date = moment(req.query.date, 'DD/MM/YYYY').toDate();
  let passengers = parseInt(req.query.passengers);
  let startDate = moment(date).startOf('isoweek').toDate();
  let endDate = moment(date).endOf('isoweek').toDate();

  let departureDate = {
    $gte: startDate,
    $lte: endDate
  };

  let fareClassQueries = [];
  switch (fareClass) {
  case 'Economy':
    fareClassQueries.push({
      'class.economy.availableSlot': { $gte: passengers }
    }, {
      'class.economyFlexi.availableSlot': { $gte: passengers }
    });
    break;
  case 'Business':
    fareClassQueries.push({
      'class.business.availableSlot': { $gte: passengers }
    });
    break;
  case 'First':
    fareClassQueries.push({
      'class.first.availableSlot': { $gte: passengers }
    });
    break;
  default:
    fareClassQueries.push({
      'class.economy.availableSlot': { $gte: passengers }
    });
    fareClassQuery.push({
      'class.economyFlexi.availableSlot': { $gte: passengers }
    });
    break;
  }

  let query = {
    $and: [
      { 'departure.airportCode': departure },
      { 'arrival.airportCode': arrival },
      { 'departure.date': departureDate },
      { $or: fareClassQueries }
    ]
  };

  database.getDb().collection('flights').find(query).toArray().then(flights => {
    if (flights.length > 0) {
      res.json(flights);
    } else {
      res.sendStatus(404);
    }
  }).catch(() => res.sendStatus(400));
}

export { findFlights, findFlightSchedules };
