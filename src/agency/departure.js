import database from '../data/database';

function getAllDepartures(req, res) {
  let flightCollection = database.getDb().collection('flights');
  let departureField = 'departure';
  flightCollection.distinct(departureField)
    .then(departures => res.json(departures))
    .catch(() => res.sendStatus(400));
}

export default getAllDepartures;
