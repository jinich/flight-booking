const errorCode = {
  NOT_FOUND_ERROR: 404,
  PERMISSION_ERROR: 403,
  INTERNAL_ERROR: 500
};

export default errorCode;
