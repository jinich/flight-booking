import errorCode from './error-code';

const response = {
  success: (res, content) => {
    let successContent = content || { message: 'Operation complete successfully.' };
    res.json({
      status: 'success',
      data: successContent
    });
  },
  notFoundError: (res, message) => {
    let notFoundMessage = message || 'Resource not found.';
    res.status(errorCode.NOT_FOUND_ERROR).json({
      status: 'failed',
      data: {
        message: notFoundMessage
      }
    });
  },
  forbiddenError: (res, message) => {
    let forbiddenMessage = message || '403 Forbidden';
    res.status(errorCode.PERMISSION_ERROR).json({
      status: 'failed',
      data: {
        message: forbiddenMessage
      }
    });
  },
  internalError: (res, message) => {
    let errorMessage = message || 'Internal Server Error.';
    res.status(errorCode.INTERNAL_ERROR).json({
      status: 'error',
      data: {
        message: errorMessage
      }
    });
  }
};

export default response;
