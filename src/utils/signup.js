function hasNext(user, collection) {
  return collection.find(user).limit(1).hasNext();
}

function insertUser(user, collection) {
  return collection.insertOne(user);
}

export default function addUser(user, collection) {
  return hasNext(user, collection).then((alreadyExist) => {
    if (!alreadyExist) {
      return insertUser(user, collection);
    } else {
      throw new Error('User already exist.');
    }
  }).then((insertResult) => {
    if (insertResult.insertedCount === 1) {
      return user;
    } else {
      throw new Error('Can not insert.');
    }
  });
}
