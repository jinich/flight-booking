function validateSignupInfo(user) {
  let usernameIsValid = /[0-9a-zA-Z_-]{6,20}/.test(user.username);
  let passwordIsValid = /[a-zA-Z\d!#$%&? "]{8,}/.test(user.password);
  return usernameIsValid && passwordIsValid;
}

export default validateSignupInfo;
