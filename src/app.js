import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import config from './config';
import setupAdminApp from './admin';
import flightApi from './agency';

function resolvePort() {
  let mode = process.env.NODE_ENV;
  let port;
  switch (mode) {
  case 'production':
    port = config.production.port;
    break;
  case 'development':
    port = config.development.port;
    break;
  case 'test':
    port = config.test.port;
    break;
  default:
    port = config.development.port;
    break;
  }
  return port;
}

let defaultPort = resolvePort();
let app = express();
let server = null;

app.set('port', process.env.PORT || defaultPort);
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));
app.set('secret', config.secret);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, '..', 'public')));

app.get('/', (req, res) => {
  res.render('home');
});

app.post('/flights/search', (req, res) => {
  let context = {
    oneWayFlights: req.body.oneWayFlights,
    returnFlights: req.body.returnFlights,
    userSelectionData: req.body.userSelectionData
  }
  res.render('flight-list', context);
});

app.start = () => {
  let port = app.get('port');
  return new Promise((resolve, reject) => {
    server = app.listen(port, (err) => {
      if (!err) {
        setupAdminApp(app);
        flightApi(app);
        resolve(port);
      } else {
        reject(err);
      }
    });
  });
};

app.stop = () => {
  server.close();
};

module.exports = app;
