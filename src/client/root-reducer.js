import { combineReducers } from 'redux';
import { search } from './search';

const rootApp = combineReducers({
  search
});

export default rootApp;
