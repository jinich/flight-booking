import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import ReactDOM from 'react-dom';
import Search from './search/Search';
import Booking from './book/Booking';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={Search} />
    <Route path="/flights/search" component={Booking} />
  </Router>
), document.getElementById('app'));
