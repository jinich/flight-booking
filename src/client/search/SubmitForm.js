import React from 'react';

class SubmitForm extends React.Component {
  componentDidMount() {
    const $submitForm = $('.submit-form');
    if ($submitForm.length) {
      $submitForm.submit();
      console.log('submitted');
    }
  }
  render() {
    return (
      <form action="/flights/search" method="POST" className="submit-form">
        <input type="hidden" value={JSON.stringify(this.props.oneWayFlights)} name="oneWayFlights" />
        <input type="hidden" value={JSON.stringify(this.props.returnFlights)} name="returnFlights" />
        <input type="hidden" value={JSON.stringify(this.props.userSelectionData)} name="userSelectionData" />
      </form>
    )
  }
}

export default SubmitForm;
