import React from 'react';
import { connect } from 'react-redux';
import { Schedule } from './schedule';
import { Passenger } from './passenger';
import { SearchFlightButton } from './SearchFlightButton';
import { searchFlights } from './search-actions';
import store from '..';

const SearchFlightForm = () => (
  <form className="search-flight-form">
    <Schedule />
    <Passenger />
    <SearchFlightButton />
  </div>
);

function mapDispatchToProps(dispatch) {
  return {
    onViewShedulesClick() {

    },
    onSearchClick() {
      dispatch(searchFlights());
    }
  }
}

export default connect(mapDispatchToProps())(SearchFlightForm);
