import { combineReducers } from 'redux';
import { schedule } from './schedule';
import { passenger } from './passenger';

const search = combineReducers({
  schedule,
  passenger
});

export default search;
