import React from 'react';

class ActionTab extends React.Component {
  render() {
    return (
      <ul className='action-tab'>
        <li className='active'><a href='#' title='Search flights'>Search flights</a></li>
        <li><a href='#' title='Manage booking'>Manage booking</a></li>
        <li><a href='#' title='Flight status'>Flight status</a></li>
      </ul>
    );
  }
}

export default ActionTab;
