import React from 'react';
import SearchFlightForm from './SearchFlightForm';

const App = () => (
  <SearchFlightForm />
);

export default App;
