import React from 'react';

const SearchFlightButton = ({ onViewShedulesClick, onSearchClick }) => (
  <div className="search-buttons">
    <input className="view-schedules-btn" type="submit" value="view shedules" onClick={() => onViewShedulesClick()} />
    <input className="search-btn" type="submit" value="search" onClick={(event) => onSearchClick()} />
  </div>
);

export default SearchFlightButton;
