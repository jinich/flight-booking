import React from 'react';

const FlightDate = ({ departDate, onDepartDateChange, returnDate, onReturnDateChange }) => (
  <div className="flight-date">
    <input
      type="date"
      required="required"
      className="depart-date"
      value={departDate}
      onChange={event => onDepartDateChange(event.target.value)} />
    <input
      type="date"
      required="required"
      className="return-date"
      value={returnDate}
      onChange={event => onReturnDateChange(event.target.value)} />
  </div>
);

export default FlightDate;
