export const CHOOSE_DEPARTURE = 'CHOOSE_DEPARTURE';
export function chooseDeparture(departureAirportCode) {
  return {
    type: CHOOSE_DEPARTURE,
    departureAirportCode
  }
}

export const REQUEST_DEPARTURES = 'REQUEST_DEPARTURES';
function requestDepartures() {
  return {
    type: REQUEST_DEPARTURES
  }
}

export const RECEIVE_DEPARTURES = 'RECEIVE_DEPARTURES';
function receiveDepartures(departures) {
  return {
    type: RECEIVE_DEPARTURES,
    departures
  }
}

export function fetchDepartures() {
  return function (dispatch) {
    dispatch(requestDepartures());
    return $.ajax('/api/flights/departures')
      .done(departures => dispatch(receiveDepartures(departures)))
      .fail(() => console.log('receive departures error!'));
  }
}

export const CHOOSE_ARRIVAL = 'CHOOSE_ARRIVAL';
export function chooseArrival(arrivalAirportCode) {
  return {
    type: CHOOSE_ARRIVAL,
    arrivalAirportCode
  }
}

export const REQUEST_ARRIVALS = 'REQUEST_ARRIVALS';
function requestArrivals() {
  return {
    type: REQUEST_ARRIVALS
  }
}

export const RECEIVE_ARRIVALS = 'RECEIVE_ARRIVALS';
function receiveArrivals(arrivals) {
  return {
    type: RECEIVE_ARRIVALS,
    arrivals
  }
}

export function fetchArrivals(departureAirportCode) {
  return function (dispatch) {
    dispatch(requestArrivals());
    return $.ajax('/api/flights/arrivals/' + departureAirportCode)
      .done(arrivals => dispatch(receiveArrivals(arrivals)))
      .fail(() => console.log('receive arrivals failed!'));
  }
}

export const CHOOSE_FLIGHT_METHOD = 'CHOOSE_FLIGHT_METHOD';
export function chooseFlightMethod(flightMethod) {
  return {
    type: CHOOSE_FLIGHT_METHOD,
    flightMethod
  }
}

export const CHOOSE_DEPART_DATE = 'CHOOSE_DEPART_DATE';
export function chooseDepartDate(departDate) {
  return {
    type: CHOOSE_DEPART_DATE,
    departDate
  }
}

export const CHOOSE_RETURN_DATE = 'CHOOSE_RETURN_DATE';
export function chooseReturnDate(returnDate) {
  return {
    type: CHOOSE_RETURN_DATE,
    returnDate
  }
}

export const CHANGE_FLEXIBLE_FILTER = 'CHANGE_FLEXIBLE_FILTER';
export function changeFlexibleFilter(isFlexible) {
  return {
    type: CHANGE_FLEXIBLE_FILTER,
    isFlexible
  }
}
