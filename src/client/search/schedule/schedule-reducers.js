import { combineReducers } from 'redux';
import moment from 'moment';
import { CHOOSE_DEPARTURE,
  REQUEST_DEPARTURES,
  RECEIVE_DEPARTURES,
  CHOOSE_ARRIVAL,
  REQUEST_ARRIVALS,
  RECEIVE_ARRIVALS,
  CHOOSE_FLIGHT_METHOD,
  CHOOSE_DEPART_DATE,
  CHOOSE_RETURN_DATE,
  CHANGE_FLEXIBLE_FILTER
} from './schedule-actions';

function defaultDepartDate() {
  return moment().add(1, 'days').format('YYYY-MM-DD');
}

function defaultReturnDate() {
  return moment().add(2, 'weeks').format('YYYY-MM-DD');
}

const initialState = {
  departureAirportCode: '',
  arrivalAirportCode: '',
  departures: [],
  arrivals: [],
  flightMethod: 'Return',
  departDate: defaultDepartDate(),
  returnDate: defaultReturnDate(),
  isFlexible: false
};

export default function schedule(state = initialState, action) {
  switch (action.type) {
  case CHOOSE_DEPARTURE:
    return Object.assign({}, state, {
      departureAirportCode: action.departureAirportCode
    });
  case RECEIVE_DEPARTURES:
    return Object.assign({}, state, {
      departures: action.departures
    });
  case CHOOSE_ARRIVAL:
    return Object.assign({}, state, {
      arrivalAirportCode: action.arrivalAirportCode
    });
  case RECEIVE_ARRIVALS:
    console.log(state);
    return Object.assign({}, state, {
      arrivals: action.arrivals
    });
  case CHOOSE_FLIGHT_METHOD:
    return Object.assign({}, state, {
      flightMethod: action.flightMethod
    });
  case CHOOSE_DEPART_DATE:
    return Object.assign({}, state, {
      departDate: action.departDate
    });
  case CHOOSE_RETURN_DATE:
    return Object.assign({}, state, {
      returnDate: action.returnDate
    });
  case CHANGE_FLEXIBLE_FILTER:
    return Object.assign({}, state, {
      isFlexible: action.isFlexible
    });
  default:
    return state;
  }
}
