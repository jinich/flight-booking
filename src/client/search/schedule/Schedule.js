import React from 'react';
import { connect } from 'react-redux';
import { fetchDepartures,
  chooseDeparture,
  fetchArrivals,
  chooseArrival,
  chooseFlightMethod,
  chooseDepartDate,
  chooseReturnDate,
  changeFlexibleFilter
} from './schedule-actions';
import Location from './Location';
import FlightMethod from './FlightMethod';
import FlightDate from './FlightDate';
import FlexibleFilter from './FlexibleFilter';

const Schedule = (props) => (
  <div className="flight-schedule">
    <Location
      departures={props.departures}
      departureAirportCode={props.departureAirportCode}
      loadDepartures={props.loadDepartures}
      onDepartureChange={props.onDepartureChange}
      arrivals={props.arrivals}
      arrivalAirportCode={props.arrivalAirportCode}
      onArrivalChange={props.onArrivalChange} />
    <FlightMethod
      flightMethod={props.flightMethod}
      onFlightMethodChange={props.onFlightMethodChange} />
    <FlightDate
      departDate={props.departDate}
      onDepartDateChange={props.onDepartDateChange}
      returnDate={props.returnDate}
      onReturnDateChange={props.onReturnDateChange} />
    <FlexibleFilter
      isFlexible={props.isFlexible}
      onFlexibleFilterChange={props.onFlexibleFilterChange} />
  </div>
);

function mapStateToProps(state) {
  const scheduleState = state.search.schedule;
  return {
    departures: scheduleState.departures,
    departureAirportCode: scheduleState.departureAirportCode,
    arrivals: scheduleState.arrivals,
    arrivalAirportCode: scheduleState.arrivalAirportCode,
    flightMethod: scheduleState.flightMethod,
    departDate: scheduleState.departDate,
    returnDate: scheduleState.returnDate,
    isFlexible: scheduleState.isFlexible
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadDepartures: () => {
      dispatch(fetchDepartures());
    },
    onDepartureChange: (departureAirportCode) => {
      dispatch(chooseDeparture(departureAirportCode));
      dispatch(fetchArrivals(departureAirportCode));
    },
    onArrivalChange: (arrivalAirportCode) => {
      dispatch(chooseArrival(arrivalAirportCode));
    },
    onFlightMethodChange: (flightMethod) => {
      dispatch(chooseFlightMethod(flightMethod));
    },
    onDepartDateChange: (departDate) => {
      dispatch(chooseDepartDate(departDate));
    },
    onReturnDateChange: (returnDate) => {
      dispatch(chooseReturnDate(returnDate));
    },
    onFlexibleFilterChange: (isFlexible) => {
      dispatch(changeFlexibleFilter(isFlexible));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
