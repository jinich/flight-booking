import React from 'react';

class Location extends React.Component {
  componentDidMount() {
    this.props.loadDepartures();
  }
  createDepartureOptions() {
    return this.props.departures.map((departure) => (
      <option value={departure.airportCode} key={departure.airportCode}>
        {`${departure.city}, ${departure.country} (${departure.airportName} - ${departure.airportCode})`}
      </option>
    ));
  }
  createArrivalOptions() {
    return this.props.arrivals.map((arrival) => (
      <option value={arrival.airportCode} key={arrival.airportCode}>
        {`${arrival.city}, ${arrival.country} (${arrival.airportName} - ${arrival.airportCode})`}
      </option>
    ));
  }
  render() {
    return (
      <div className="schedule">
        <div className="from schedule-select">
          <select
            className="departure"
            value={this.props.departureAirportCode}
            onChange={event => this.props.onDepartureChange(event.target.value)}>
            <option value="" disabled>I am from here...</option>
            {this.createDepartureOptions()}
          </select>
        </div>
        <div className="to schedule-select">
          <select
            className="arrival"
            value={this.props.arrivalAirportCode}
            onChange={event => this.props.onArrivalChange(event.target.value)}>
            <option value="" disabled>And i want to go to...</option>
            {this.createArrivalOptions()}
          </select>
        </div>
      </div>
    );
  }
}

export default Location;
