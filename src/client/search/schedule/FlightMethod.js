import React from 'react';

const FlightMethod = ({ flightMethod, onFlightMethodChange }) => {
  const method = {
    return: 'Return',
    oneWay: 'One-way'
  };
  return (
    <div className="flight-method">
      <div className="return">
        <input
          type="radio" name="flight-method" id="return"
          value={method.return}
          checked={flightMethod === method.return}
          onChange={event => onFlightMethodChange(event.target.value)} />
        <label htmlFor="return">Return</label>
      </div>
      <div className="one-way">
        <input type="radio" name="flight-method" id="one-way"
          value={method.oneWay}
          checked={flightMethod === method.oneWay}
          onChange={event => onFlightMethodChange(event.target.value)} />
        <label htmlFor="one-way">One-way</label>
      </div>
    </div>
  );
}

export default FlightMethod;
