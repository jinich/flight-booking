import React from 'react';

const FlexibleFilter = ({ isFlexible, onFlexibleFilterChange }) => (
  <div className="filter">
    <input
      type="checkbox"
      id="flexible-filter"
      checked={isFlexible}
      onChange={event => onFlexibleFilterChange(event.target.checked)} />
    <label htmlFor="flexible-filter">Flexible travel dates / Lowest fares</label>
  </div>
);

export default FlexibleFilter;
