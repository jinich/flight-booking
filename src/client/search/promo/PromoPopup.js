import React from 'react';

class PromoPopup extends React.Component {
  render() {
    return (
      <div className="promo-popup">
        <h1>Enter promo code</h1>
        <p>If you have a promo code for a discount, upgrade or any other offer, enter it here.</p>
        <form>
          <input type="text" />
          <input type="submit" value="apply" />
        </form>
      </div>
    );
  }
}

export default PromoPopup;
