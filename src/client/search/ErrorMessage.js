import React from 'react';
import ReactDOM from 'react-dom';

const ErrorMessage = ({message}) => {
  const style = {
    'white-space': 'pre-wrap'
  };
  return (
    <div className="error-message">
      <p style={style}>{message}</p>
    </div>
  );
};

export default ErrorMessage;
