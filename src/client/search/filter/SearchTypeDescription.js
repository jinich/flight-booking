import React from 'react';

class SearchTypeDescription extends React.Component {
  render() {
    return (
      <div className="search-type-desc">
        <span></span>
        <p>KrisFlyer/PPS Club members have the option to combine KrisFlyer miles with their credit/debit card for payment.</p>
      </div>
    );
  }
}

export default SearchTypeDescription;
