import React from 'react';

class SearchType extends React.Component {
  render() {
    return (
      <div className="search-type">
        <div className="book">
          <input type="radio" name="search-type" id="book-flights" />
          <label htmlFor="book-flights">Book flights</label>
        </div>
        <div className="redeem">
          <input type="radio" name="search-type" id="redeem-flights" />
          <label htmlFor="redeem-flights">Redeem flights</label>
        </div>
      </div>
    );
  }
}

export default SearchType;
