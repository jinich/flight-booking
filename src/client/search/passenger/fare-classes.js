const fareClasses = {
  economy: 'Economy / Premium Economy',
  business: 'Business',
  first: 'First / Suites'
};

export default fareClasses;
