import {
  CHOOSE_FARE_CLASS,
  CHOOSE_ADULT_SLOT,
  CHOOSE_CHILD_SLOT,
  CHOOSE_INFANT_SLOT
} from './passenger-actions';
import fareClasses from './fare-classes';

const initialState = {
  fareClass: fareClasses.economy,
  adultSlot: 1,
  adultAvailableSlot: 9,
  childSlot: 0,
  childAvailableSlot: 5,
  infantSlot: 0,
  infantAvailableSlot: 1
};

export default function passenger(state = initialState, action) {
  switch (action.type) {
  case CHOOSE_FARE_CLASS:
    return Object.assign({}, state, {
      fareClass: action.fareClass
    });
  case CHOOSE_ADULT_SLOT:
    return Object.assign({}, state, {
      adultSlot: action.adultSlot
    });
  case CHOOSE_CHILD_SLOT:
    return Object.assign({}, state, {
      childSlot: action.childSlot
    });
  case CHOOSE_INFANT_SLOT:
    return Object.assign({}, state, {
      infantSlot: action.infantSlot
    });
  default:
    return state;
  }
}
