import React from 'react';
import { connect } from 'react-redux';
import FareClass from './FareClass';
import PassengerCategory from './PassengerCategory';
import {
  chooseFareClass,
  chooseAdultSlot,
  chooseChildSlot,
  chooseInfantSlot
} from './passenger-actions';

const Passenger = (props) => (
  <div className="passenger">
    <FareClass
      fareClass={props.fareClass}
      onFareClassChange={props.onFareClassChange} />
    <PassengerCategory
      adultAvailableSlot={props.adultAvailableSlot}
      adultSlot={props.adultSlot}
      onAdultSlotChange={props.onAdultSlotChange}
      childAvailableSlot={props.childAvailableSlot}
      childSlot={props.childSlot}
      onChildSlotChange={props.onChildSlotChange}
      infantAvailableSlot={props.infantAvailableSlot}
      infantSlot={props.infantSlot}
      onInfantSlotChange={props.onInfantSlotChange} />
  </div>
);

function mapStateToProps(state) {
  const passengerState = state.search.passenger;
  return {
    fareClass: passengerState.fareClass,
    adultAvailableSlot: passengerState.adultAvailableSlot,
    adultSlot: passengerState.adultSlot,
    childAvailableSlot: passengerState.childAvailableSlot,
    childSlot: passengerState.childSlot,
    infantAvailableSlot: passengerState.infantAvailableSlot,
    infantSlot: passengerState.infantSlot
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onFareClassChange: (fareClass) => {
      dispatch(chooseFareClass(fareClass));
    },
    onAdultSlotChange: (adultSlot) => {
      dispatch(chooseAdultSlot(adultSlot));
    },
    onChildSlotChange: (childSlot) => {
      dispatch(chooseChildSlot(childSlot));
    },
    onInfantSlotChange: (infantSlot) => {
      dispatch(chooseInfantSlot(infantSlot));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Passenger);
