export const CHOOSE_FARE_CLASS = 'CHOOSE_FARE_CLASS';
export function chooseFareClass(fareClass) {
  return {
    type: CHOOSE_FARE_CLASS,
    fareClass
  };
}

export const CHOOSE_ADULT_SLOT = 'CHOOSE_ADULT_SLOT';
export function chooseAdultSlot(adultSlot) {
  return {
    type: CHOOSE_ADULT_SLOT,
    adultSlot
  };
}

export const CHOOSE_CHILD_SLOT = 'CHOOSE_CHILD_SLOT';
export function chooseChildSlot(childSlot) {
  return {
    type: CHOOSE_CHILD_SLOT,
    childSlot
  };
}

export const CHOOSE_INFANT_SLOT= 'CHOOSE_INFANT_SLOT';
export function chooseInfantSlot(infantSlot) {
  return {
    type: CHOOSE_INFANT_SLOT,
    infantSlot
  }
}
