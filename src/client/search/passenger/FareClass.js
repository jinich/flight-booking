import React from 'react';
import fareClasses from './fare-classes';

const FareClass = ({ fareClass, onFareClassChange }) => {
  const fareClassOptions = [];
  const fareClassesClone = Object.assign({}, fareClasses);
  for (let key in fareClassesClone) {
    const option = (
      <option value={fareClassesClone[key]} key={key}>
        {fareClassesClone[key]}
      </option>
    );
    fareClassOptions.push(option);
  }
  return (
    <div className="fare-class">
      <select
        value={fareClass}
        onChange={event => onFareClassChange(event.target.value)}>
        {fareClassOptions}
      </select>
    </div>
  );
}

export default FareClass;
