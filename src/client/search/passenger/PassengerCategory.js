import React from 'react';

class PassengerCategory extends React.Component {
  render() {
    const adultOptions = [];
    for (let slot = 1; slot <= this.props.adultAvailableSlot; slot++) {
      const option = (
        <option value={slot} key={slot}>
          {slot}
        </option>
      );
      adultOptions.push(option);
    };
    const childOptions = [];
    for (let slot = 0; slot <= this.props.childAvailableSlot; slot++) {
      const option = (
        <option value={slot} key={slot}>
          {slot}
        </option>
      );
      childOptions.push(option);
    };
    const infantOptions = [];
    for (let slot = 0; slot <= this.props.infantAvailableSlot; slot++) {
      const option = (
        <option value={slot} key={slot}>
          {slot}
        </option>
      );
      infantOptions.push(option);
    };
    return (
      <div className="passenger-category">
        <select
          className="adult"
          value={this.props.adultSlot}
          onChange={event => this.props.onAdultSlotChange(event.target.value)}>
          {adultOptions}
        </select>
        <select
          className="child"
          value={this.props.childSlot}
          onChange={event => this.props.onChildSlotChange(event.target.value)}>
          {childOptions}
        </select>
        <select
          className="infant"
          value={this.props.infantSlot}
          onChange={event => this.props.onInfantSlotChange(event.target.value)}>
          {infantOptions}
        </select>
      </div>
    );
  }
}

export default PassengerCategory;
