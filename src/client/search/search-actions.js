export const REQUEST_FLIGHTS = 'REQUEST_FLIGHTS';
function requestFlights() {
  return {
    type: REQUEST_FLIGHTS
  }
}

export const RECEIVE_FLIGHTS = 'RECEIVE_FLIGHTS';
function receiveFlights(flights) {
  return {
    type: RECEIVE_FLIGHTS,
    flights
  }
}

export const REQUEST_FLIGHTS_FAILED = 'REQUEST_FLIGHTS_FAILED';
function requestFlightsFailed() {
  return {
    type: REQUEST_FLIGHTS_FAILED
  }
}

export function searchFlights() {
  return function dispatch() {
    dispatch(requestFlights());
    return $.ajax('')
      .done(flights => dispatch(receiveFlights(flights)))
      .fail(() => dispatch(REQUEST_FLIGHTS_FAILED));
  }
}
