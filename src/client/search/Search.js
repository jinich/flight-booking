import React from 'react';
import ActionTab from './ActionTab';
import SearchType from './SearchType';
import SearchFlightForm from './SearchFlightForm';
import ErrorMessage from './ErrorMessage';

class Search extends React.Component {
  constructor() {
    super();
    this.state = {
      hasError: false,
      errorMessage: '',
      submitForm: null
    };
  }
  onError(message) {
    this.setState({
      hasError: true,
      errorMessage: message
    });
  }
  onSubmitForm(form) {
    this.setState({submitForm: form});
  }
  render() {
    const error = this.state.hasError ? <ErrorMessage message={this.state.errorMessage} /> : null;
    return (
      <div className="booking-search">
        <ActionTab />
        <div className="tab-wrapper">
          {error}
          <SearchType />
          <SearchFlightForm
            onError={(message) => this.onError(message)}
            onSubmitForm={(form) => this.onSubmitForm(form)} />
          {this.state.submitForm}
        </div>
      </div>
    );
  }
}

export default Search;
