import React from 'react';
import moment from 'moment';
import Schedule from './Schedule';
import SearchTypeDescription from './SearchTypeDescription';
import FlightMethod from './FlightMethod';
import FlexibleFilter from './FlexibleFilter';
import FlightDate from './FlightDate';
import FareClass from './FareClass';
import PassengerCategory from './PassengerCategory';
import Promo from './Promo';
import PromoPopup from './PromoPopup';
import SearchFlightButton from './SearchFlightButton';
import SubmitForm from './SubmitForm';

function defaultDepartDate() {
  return moment().add(1, 'days').format('YYYY-MM-DD');
}

function defaultReturnDate() {
  return moment().add(2, 'weeks').format('YYYY-MM-DD');
}

class SearchFlightForm extends React.Component {
  constructor() {
    super();

    this.fareClasses = {
      economy: 'Economy / Premium Economy',
      business: 'Business',
      first: 'First / Suites'
    };

    this.maximumSlot = {
      all: 9,
      adult: 9,
      child: 5,
      infant: 1
    }

    this.state = {
      departures: [],
      arrivals: [],
      departureAirportCode: '',
      arrivalAirportCode: '',
      flightMethod: 'Return',
      isFlexible: false,
      departDate: defaultDepartDate(),
      returnDate: defaultReturnDate(),
      fareClass: this.fareClasses.economy,
      adultSlot: 1,
      availableAdultSlot: 9,
      childSlot: 0,
      availableChildSlot: 5,
      infantSlot: 0,
      availableInfantSlot: 1
    };
  }
  componentDidMount() {
    $.ajax('/api/flights/departures', {
      success: (departures) => {
        this.setState({departures: departures});
      },
      error: (() => {
        this.setState({departures: []});
      })
    });
  }
  onDepartureChange(departureAirportCode) {
    this.setState({departureAirportCode: departureAirportCode});
    $.ajax('/api/flights/arrivals/' + departureAirportCode, {
      success: (arrivals) => {
        this.setState({arrivals: arrivals})
      },
      error: () => {
        this.setState({arrivals: []});
      }
    });
  }
  onArrivalChange(arrivalAirportCode) {
    this.setState({arrivalAirportCode: arrivalAirportCode});
  }
  onFlightMethodChange(method) {
    this.setState({flightMethod: method});
  }
  onFlexibleFilterChange(isFlexible) {
    this.setState({isFlexible: isFlexible});
  }
  onDepartDateChange(departDate) {
    this.setState({departDate: departDate});
  }
  onReturnDateChange(returnDate) {
    this.setState({returnDate: returnDate});
  }
  onFareClassChange(fareClass) {
    this.setState({fareClass: fareClass});
  }
  onViewShedulesClick() {

  }
  onSearchClick(event) {
    event.preventDefault();
    let fareClass = '';
    switch (this.state.fareClass) {
    case this.fareClasses.economy:
      fareClass = 'Economy';
      break;
    case this.fareClasses.business:
      fareClass = 'Business';
      break;
    case this.fareClasses.first:
      fareClass = 'First';
      break;
    default:
      break;
    }
    const passengers = this.state.adultSlot + this.state.childSlot + this.state.infantSlot;
    const date = moment(this.state.departDate, 'YYYY-MM-DD').format('DD/MM/YYYY');

    let inputError = '';
    if (this.state.departureAirportCode === '') {
      inputError = 'Departure must be choosen!\n';
    }
    if (this.state.arrivalAirportCode === '') {
      inputError += 'Arrival must be choosen!\n';
    }
    if (inputError === '') {
      const requestUrl = `/api/flights?departure=${this.state.departureAirportCode}&arrival=${this.state.arrivalAirportCode}&date=${date}&class=${fareClass}&flexible=false&passengers=${passengers}`;
      $.ajax(requestUrl, {
        success: (oneWayFlights) => {
          const userSelectionData = {
            fareClass: this.state.fareClass,
            flightMethod: this.state.flightMethod
          };
          if (this.state.flightMethod === 'One-way') {
            const flightsSubmitForm = (
              <SubmitForm
                oneWayFlights={oneWayFlights}
                returnFlights={[]}
                userSelectionData={userSelectionData} />
            );
            this.props.onSubmitForm(flightsSubmitForm);
          } else {
            // return
            const returnDate = moment(this.state.returnDate, 'YYYY-MM-DD').format('DD/MM/YYYY');
            const returnRequestUrl = `/api/flights?departure=${this.state.arrivalAirportCode}&arrival=${this.state.departureAirportCode}&date=${returnDate}&class=${fareClass}&flexible=false&passengers=${passengers}`;
            $.ajax(returnRequestUrl, {
              success: (returnFlights) => {
                const flightsSubmitForm = (
                  <SubmitForm
                    oneWayFlights={oneWayFlights}
                    returnFlights={returnFlights}
                    userSelectionData={userSelectionData} />
                );
                this.props.onSubmitForm(flightsSubmitForm);
              },
              error: () => {
                const flightsSubmitForm = (
                  <SubmitForm
                    oneWayFlights={oneWayFlights}
                    returnFlights={[]}
                    userSelectionData={userSelectionData} />
                );
                this.props.onSubmitForm(flightsSubmitForm);
              }
            });
          }
        },
        error: () => {
          this.props.onError('Sorry, we has no flights lefts!');
        }
      });
    } else {
      this.props.onError(inputError);
    }
  }
  updateAvailableSlot() {
    let availableAdultSlot = this.maximumSlot.all - this.state.childSlot - this.state.infantSlot;
    availableAdultSlot = availableAdultSlot <= this.maximumSlot.adult ? availableAdultSlot : 0;
    this.setState({availableAdultSlot: availableAdultSlot});

    let availableChildSlot = this.maximumSlot.all - this.state.adultSlot - this.state.infantSlot;
    availableChildSlot = availableChildSlot <= this.maximumSlot.child ? availableChildSlot : 0;
    this.setState({availableChildSlot: availableChildSlot});

    let availableInfantSlot = this.maximumSlot.all - this.state.adultSlot - this.state.infantSlot;
    availableInfantSlot = availableInfantSlot <= this.maximumSlot.infant ? availableInfantSlot : 0;
    this.setState({availableInfantSlot: availableInfantSlot});
  }
  onAdultSlotChange(slot) {
    this.setState({adultSlot: slot});
  }
  onChildSlotChange(slot) {
    this.setState({childSlot: slot});
  }
  onInfantSlotChange(slot) {
    this.setState({infantSlot: slot});
  }
  render() {
    return (
      <div className="search-flight-form">
        <SearchTypeDescription />
        <form method="GET" className="search-flights">
          <Schedule
            departures={this.state.departures}
            arrivals={this.state.arrivals}
            departureAirportCode={this.state.departureAirportCode}
            onDepartureChange={departureAirportCode => this.onDepartureChange(departureAirportCode)}
            arrivalAirportCode={this.state.arrivalAirportCode}
            onArrivalChange={arrivalAirportCode => this.onArrivalChange(arrivalAirportCode)} />
          <FlightMethod
            method={this.state.flightMethod}
            onFlightMethodChange={(method) => this.onFlightMethodChange(method)} />
          <FlexibleFilter
            isFlexible={this.state.isFlexible}
            onFlexibleFilterChange={isFlexible => this.onFlexibleFilterChange(isFlexible)} />
          <FlightDate
            departDate={this.state.departDate}
            onDepartDateChange={departDate => this.onDepartDateChange(departDate)}
            returnDate={this.state.returnDate}
            onReturnDateChange={returnDate => this.onReturnDateChange(returnDate)} />
          <FareClass
            fareClasses={this.fareClasses}
            fareClass={this.state.fareClass}
            onFareClassChange={fareClass => this.onFareClassChange(fareClass)} />
          <PassengerCategory
            adultAvailableSlot={this.state.availableAdultSlot}
            adultSlot={this.state.adultSlot}
            onAdultSlotChange={slot => this.onAdultSlotChange(slot)}
            childAvailableSlot={this.state.availableChildSlot}
            childSlot={this.state.childSlot}
            onChildSlotChange={slot => this.onChildSlotChange(slot)}
            infantAvailableSlot={this.state.availableInfantSlot}
            infantSlot={this.state.infantSlot}
            onInfantSlotChange={slot => this.onInfantSlotChange(slot)} />
          <Promo />
          <SearchFlightButton
            onViewShedulesClick={() => this.onViewShedulesClick()}
            onSearchClick={(event) => this.onSearchClick(event)} />
        </form>
      </div>
    );
  }
}

export default SearchFlightForm;
