import React from 'react';
import moment from 'moment';

class FlightRow extends React.Component {
  render() {
    const flight = this.props.flight;
    const departureDate = moment(flight.departure.date).format('Do MMM (ddd)');
    const arrivalDate = moment(flight.arrival.date).format('Do MMM (ddd)');
    const baseClass = this.props.fareClass.split(' ')[0];
    let fareClasses = [];
    if (baseClass === 'Economy') {
      fareClasses.push(
        <td key="economy-saver">
          <div className="flexi-saver">
            <input type="radio" id="flight-select" name="flight-select" />
            <label htmlFor="flight-select" className="class-desc">
              <span>From</span>
              <span>{flight.class.economyFlexi.standardFare}</span>
              <span>Total fare, 1 adult</span>
            </label>
          </div>
        </td>,
        <td key="economy">
          <div className="flexi">
            <input type="radio" id="flight-select" name="flight-select" />
            <label htmlFor="flight-select" className="class-desc">
              <span>From</span>
              <span>{flight.class.economy.standardFare}</span>
              <span>Total fare, 1 adult</span>
            </label>
          </div>
        </td>
      );
    } else if (baseClass === 'Business') {
      fareClasses.push(
        <td>
          <div className="business">
            <input type="radio" id="flight-select" name="flight-select" />
            <label htmlFor="flight-select" className="class-desc">
              <span>From</span>
              <span>{flight.class.business.standardFare}</span>
              <span>Total fare, 1 adult</span>
            </label>
          </div>
        </td>
      );
    } else if (baseClass === 'First') {
      fareClasses.push(
        <td>
          <div className="first">
            <input type="radio" id="flight-select" name="flight-select" />
            <label htmlFor="flight-select" className="class-desc">
              <span>From</span>
              <span>{flight.class.first.standardFare}</span>
              <span>Total fare, 1 adult</span>
            </label>
          </div>
        </td>
      );
    }
    return (
      <tr className="flight">
        <td className="flight-info">
          <div className="brief-info">
            <div className="airplane-desc">
              <h1>FLIGHT {flight.code}</h1>
              <span>Aircraft type: {flight.aircraft.name} {flight.aircraft.code}</span>
              <span>Flying time: {flight.totalTravelTime}</span>
            </div>
            <span className="fare-class">{baseClass}</span>
          </div>
          <div className="info">
            <div className="departure">
              <h1>{flight.departure.airportCode} {flight.departure.time}</h1>
              <span>{flight.departure.city}</span>
              <span>{departureDate},</span>
              <span>{flight.departure.airportName}</span>
              <span>Singapore Airlines</span>
            </div>
            <div className="arrival">
              <h1>{flight.arrival.airportCode} {flight.arrival.time}</h1>
              <span>{flight.arrival.city}</span>
              <span>{arrivalDate},</span>
              <span>{flight.departure.airportName}</span>
            </div>
          </div>
          <span className="travel-time">Total travel time: {flight.totalTravelTime}</span>
        </td>
        {fareClasses}
      </tr>
    );
  }
}

export default FlightRow;
