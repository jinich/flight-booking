import React from 'react';
import FlightList from './FlightList';

class Booking extends React.Component {
  render() {
    return (
      <div className="booking">
        <FlightList />
      </div>
    )
  }
}

export default Booking;
