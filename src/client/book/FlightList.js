import React from 'react';
import FlightTable from './FlightTable';

class FlightList extends React.Component {
  render() {
    const oneWayFlights = JSON.parse(document.getElementById('one-way-flights').value);
    const userSelectionData = JSON.parse(document.getElementById('user-selection-data').value);
    let returnNode = null;
    if (userSelectionData.flightMethod === 'Return') {
      const returnFlights = JSON.parse(document.getElementById('return-flights').value);
      returnNode = (
        <div className="return">
          <h1>{oneWayFlights[0].arrival.city} to {oneWayFlights[0].departure.city}</h1>
          <FlightTable
            flights={returnFlights}
            fareClass={userSelectionData.fareClass} />
        </div>
      );
    }
    return (
      <div className="flight-list">
        <div className="one-way">
          <h1>{oneWayFlights[0].departure.city} to {oneWayFlights[0].arrival.city}</h1>
          <FlightTable
            flights={oneWayFlights}
            fareClass={userSelectionData.fareClass} />
        </div>
        {returnNode}
      </div>
    );
  }
}

export default FlightList;
