import React from 'react';

class FlightHead extends React.Component {
  render() {
    const baseClass = this.props.fareClass.split(' ')[0];
    let fareClasses = [];
    if (baseClass === 'Economy') {
      fareClasses.push(<th key="flexi-saver">Economy Flexi Saver</th>, <th key="flexi">Economy Flexi</th>);
    } else {
      fareClasses.push(<th key={baseClass}>{baseClass}</th>);
    }
    return (
      <tr>
        <th>Flight Information</th>
        {fareClasses}
      </tr>
    );
  }
}

export default FlightHead;
