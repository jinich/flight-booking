import React from 'react';
import FlightRow from './FlightRow';
import FlightHead from './FlightHead';

class FlightTable extends React.Component {
  render() {
    const flightRows= this.props.flights.map(flight =>
       <FlightRow flight={flight} fareClass={this.props.fareClass} key={flight.code} />
    );
    return (
      <table className="flight-table">
        <thead>
          <FlightHead fareClass={this.props.fareClass} />
        </thead>
        <tbody>
          {flightRows}
        </tbody>
      </table>
    );
  }
}

export default FlightTable;
