import app from './src/app';
import database from './src/data/database';

Promise.all([database.connect(), app.start()]).then(([status, port]) => {
  console.log("Server is listening on port", port);
}).catch(err => {
  console.log(err);
});
