module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "rules": {
      // override rules from base configurations
      "no-else-return": 0,
      "prefer-const": 0,
      "no-console": 0,
      "linebreak-style": [2, "windows"],
      "indent": [2, 2, {"SwitchCase": 0}],
      "comma-dangle": [2, 0]
    }
};
