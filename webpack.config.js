const path = require('path');

module.exports = {
  context: path.resolve(__dirname, 'src/client'),
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: 'main.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      }
    ]
  }
}
